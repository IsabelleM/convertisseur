import pandas as pd
import json
import xml.etree.ElementTree as ET
from sqlalchemy import create_engine

def convert_file(input_file, output_format, output_file):
    try:
        if output_format.lower() == 'json':
            # Conversion vers JSON
            if input_file.lower().endswith('.csv'):
                df = pd.read_csv(input_file)
            elif input_file.lower().endswith('.xlsx') or input_file.lower().endswith('.xls'):
                df = pd.read_excel(input_file)
            elif input_file.lower().endswith('.xml'):
                tree = ET.parse(input_file)
                root = tree.getroot()
                data = []
                for record in root:
                    data.append({child.tag: child.text for child in record})
                df = pd.DataFrame(data)
            elif input_file.lower().endswith('.sql'):
                engine = create_engine('sqlite:///' + input_file)
                table_name = input('Enter Table name:')
                df = pd.read_sql_table(table_name, con=engine)
            else:
                raise ValueError("Format de fichier d'entrée non pris en charge.")

            df.to_json(output_file, orient='records', lines=True)

        elif output_format.lower() == 'csv':
            # Conversion vers CSV
            if input_file.lower().endswith('.json'):
                df = pd.read_json(input_file)
            elif input_file.lower().endswith('.xlsx') or input_file.lower().endswith('.xls'):
                df = pd.read_excel(input_file)
            elif input_file.lower().endswith('.xml'):
                tree = ET.parse(input_file)
                root = tree.getroot()
                data = []
                for record in root:
                    data.append({child.tag: child.text for child in record})
                df = pd.DataFrame(data)
            elif input_file.lower().endswith('.sql'):
                engine = create_engine('sqlite:///' + input_file)
                table_name = input('Enter Table name:')
                df = pd.read_sql_table(table_name, con=engine)
            else:
                raise ValueError("Format de fichier d'entrée non pris en charge.")

            df.to_csv(output_file, index=False)

        elif output_format.lower() == 'excel':
            # Conversion vers Excel
            if input_file.lower().endswith('.json'):
                df = pd.read_json(input_file)
            elif input_file.lower().endswith('.csv'):
                df = pd.read_csv(input_file)
            elif input_file.lower().endswith('.xml'):
                tree = ET.parse(input_file)
                root = tree.getroot()
                data = []
                for record in root:
                    data.append({child.tag: child.text for child in record})
                df = pd.DataFrame(data)
            elif input_file.lower().endswith('.sql'):
                engine = create_engine('sqlite:///' + input_file)
                table_name = input('Enter Table name:')
                df = pd.read_sql_table(table_name, con=engine)
            else:
                raise ValueError("Format de fichier d'entrée non pris en charge.")

            df.to_excel(output_file, index=False)

        elif output_format.lower() == 'xml':
            # Conversion vers XML
            if input_file.lower().endswith('.json'):
                df = pd.read_json(input_file)
            elif input_file.lower().endswith('.csv'):
                df = pd.read_csv(input_file)
            elif input_file.lower().endswith('.xlsx') or input_file.lower().endswith('.xls'):
                df = pd.read_excel(input_file)
            elif input_file.lower().endswith('.sql'):
                engine = create_engine('sqlite:///' + input_file)
                table_name = input('Enter Table name:')
                df = pd.read_sql_table(table_name, con=engine)
            else:
                raise ValueError("Format de fichier d'entrée non pris en charge.")

            root = ET.Element('data')
            for _, row in df.iterrows():
                record = ET.SubElement(root, 'record')
                for col_name, value in row.items():
                    ET.SubElement(record, col_name).text = str(value)
            tree = ET.ElementTree(root)
            tree.write(output_file)

        elif output_format.lower() == 'sql':
            # Conversion vers SQL (création d'un fichier SQL avec des instructions d'insertion)
            if input_file.lower().endswith('.json'):
                df = pd.read_json(input_file)
            elif input_file.lower().endswith('.csv'):
                df = pd.read_csv(input_file)
            elif input_file.lower().endswith('.xlsx') or input_file.lower().endswith('.xls'):
                df = pd.read_excel(input_file)
            elif input_file.lower().endswith('.xml'):
                tree = ET.parse(input_file)
                root = tree.getroot()
                data = []
                for record in root:
                    data.append({child.tag: child.text for child in record})
                df = pd.DataFrame(data)
            else:
                raise ValueError("Format de fichier d'entrée non pris en charge.")

            with open(output_file, 'w') as f:
                for _, row in df.iterrows():
                    columns = ', '.join(row.index)
                    values = ', '.join([f"'{v}'" if isinstance(v, str) else str(v) for v in row.values])
                    f.write(f"INSERT INTO {output_format} ({columns}) VALUES ({values});\n")

        else:
            raise ValueError("Format de sortie non pris en charge.")

        return f"Conversion réussie. Fichier '{output_file}' créé."

    except Exception as e:
        return f"Erreur lors de la conversion : {e}"

# Exemple d'utilisation :
input_file = 'input.csv'
output_format = 'json'
output_file = 'output.json'

result = convert_file(input_file, output_format, output_file)
print(result)

input_file = 'input.csv'
output_format = 'sql'
output_file = 'output.sql'
result = convert_file(input_file, output_format, output_file)
print(result)

input_file = 'input.csv'
output_format = 'xml'
output_file = 'output.xml'
result = convert_file(input_file, output_format, output_file)
print(result)

input_file = 'input.csv'
output_format = 'excel'
output_file = 'output.xlsx'
result = convert_file(input_file, output_format, output_file)
print(result)