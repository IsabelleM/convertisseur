INSERT INTO sql (author, title, genre, price, publish_date, description) VALUES ('Gambardella, Matthew', 'XML Developer's Guide', 'Computer', '44.95', '2000-10-01', 'An in-depth look at creating applications 
      with XML.');
INSERT INTO sql (author, title, genre, price, publish_date, description) VALUES ('Ralls, Kim', 'Midnight Rain', 'Fantasy', '5.95', '2000-12-16', 'A former architect battles corporate zombies, 
      an evil sorceress, and her own childhood to become queen 
      of the world.');
INSERT INTO sql (author, title, genre, price, publish_date, description) VALUES ('Corets, Eva', 'Maeve Ascendant', 'Fantasy', '5.95', '2000-11-17', 'After the collapse of a nanotechnology 
      society in England, the young survivors lay the 
      foundation for a new society.');
INSERT INTO sql (author, title, genre, price, publish_date, description) VALUES ('Corets, Eva', 'Oberon's Legacy', 'Fantasy', '5.95', '2001-03-10', 'In post-apocalypse England, the mysterious 
      agent known only as Oberon helps to create a new life 
      for the inhabitants of London. Sequel to Maeve 
      Ascendant.');
INSERT INTO sql (author, title, genre, price, publish_date, description) VALUES ('Corets, Eva', 'The Sundered Grail', 'Fantasy', '5.95', '2001-09-10', 'The two daughters of Maeve, half-sisters, 
      battle one another for control of England. Sequel to 
      Oberon's Legacy.');
INSERT INTO sql (author, title, genre, price, publish_date, description) VALUES ('Randall, Cynthia', 'Lover Birds', 'Romance', '4.95', '2000-09-02', 'When Carla meets Paul at an ornithology 
      conference, tempers fly as feathers get ruffled.');
INSERT INTO sql (author, title, genre, price, publish_date, description) VALUES ('Thurman, Paula', 'Splish Splash', 'Romance', '4.95', '2000-11-02', 'A deep sea diver finds true love twenty 
      thousand leagues beneath the sea.');
INSERT INTO sql (author, title, genre, price, publish_date, description) VALUES ('Knorr, Stefan', 'Creepy Crawlies', 'Horror', '4.95', '2000-12-06', 'An anthology of horror stories about roaches,
      centipedes, scorpions  and other insects.');
INSERT INTO sql (author, title, genre, price, publish_date, description) VALUES ('Kress, Peter', 'Paradox Lost', 'Science Fiction', '6.95', '2000-11-02', 'After an inadvertant trip through a Heisenberg
      Uncertainty Device, James Salway discovers the problems 
      of being quantum.');
INSERT INTO sql (author, title, genre, price, publish_date, description) VALUES ('O'Brien, Tim', 'Microsoft .NET: The Programming Bible', 'Computer', '36.95', '2000-12-09', 'Microsoft's .NET initiative is explored in 
      detail in this deep programmer's reference.');
