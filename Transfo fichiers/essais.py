from convertisseur import convert_file


# Avec un fichier csv
input_file = 'input.csv'
output_format = 'json'
output_file = 'output.json'

result = convert_file(input_file, output_format, output_file)
print(result)

input_file = 'input.csv'
output_format = 'sql'
output_file = 'output.sql'
result = convert_file(input_file, output_format, output_file)
print(result)

input_file = 'input.csv'
output_format = 'xml'
output_file = 'output.xml'
result = convert_file(input_file, output_format, output_file)
print(result)

input_file = 'input.csv'
output_format = 'excel'
output_file = 'output.xlsx'
result = convert_file(input_file, output_format, output_file)
print(result)


# Avec un fichier json
input_file = 'input.json'
output_format = 'sql'
output_file = 'output.sql'
result = convert_file(input_file, output_format, output_file)
print(result)

input_file = 'input.json'
output_format = 'xml'
output_file = 'output.xml'
result = convert_file(input_file, output_format, output_file)
print(result)

input_file = 'input.json'
output_format = 'excel'
output_file = 'output.xlsx'
result = convert_file(input_file, output_format, output_file)
print(result)

input_file = 'input.json'
output_format = 'csv'
output_file = 'output.csv'
result = convert_file(input_file, output_format, output_file)
print(result)

# Avec un fichier xml
input_file = 'input.xml'
output_format = 'sql'
output_file = 'output.sql'
result = convert_file(input_file, output_format, output_file)
print(result)

input_file = 'input.xml'
output_format = 'json'
output_file = 'output.json'
result = convert_file(input_file, output_format, output_file)
print(result)

input_file = 'input.xml'
output_format = 'excel'
output_file = 'output.xlsx'
result = convert_file(input_file, output_format, output_file)
print(result)

input_file = 'input.xml'
output_format = 'csv'
output_file = 'output.csv'
result = convert_file(input_file, output_format, output_file)
print(result)

# Avec un fichier sql
input_file = 'input.sql'
output_format = 'xml'
output_file = 'output.xml'
result = convert_file(input_file, output_format, output_file)
print(result)

input_file = 'input.sql'
output_format = 'json'
output_file = 'output.json'
result = convert_file(input_file, output_format, output_file)
print(result)

input_file = 'input.sql'
output_format = 'excel'
output_file = 'output.xlsx'
result = convert_file(input_file, output_format, output_file)
print(result)

input_file = 'input.sql'
output_format = 'csv'
output_file = 'output.csv'
result = convert_file(input_file, output_format, output_file)
print(result)