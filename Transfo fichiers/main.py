import pandas as pd
import json

def convert(path: str, format: str) -> str:
    pass


data = [
    {
        "id": 1,
        "name": "Albert",
        "address": {
            "city": "Amsterdam"
        }
    },
    {
        "id": 2,
        "name": "Adam",
        "address": {
            "city": "Paris"
        }
    },
    {
        "id": 3,
        "name": "Sara",
        "address": {
            "city": "Madrid"
        }
    }
]
# Fonctions qui convertissent un fichier en json en csv, excel, sql et xml
def convert_tocsv(data):
        json_data = json.loads(data)  # Charger les données JSON en Python
        df = pd.DataFrame(json_data)  # Créer un DataFrame à partir des données
        csv_file = 'output.csv'
        df.to_csv(csv_file, index=False)  # Écrire le DataFrame dans un fichier CSV
        return f"Conversion en CSV réussie. Fichier '{csv_file}' créé."
    

def convert_to_excel(data):
        json_data = json.loads(data)  # Charger les données JSON en Python
        df = pd.DataFrame(json_data)  # Créer un DataFrame à partir des données
        excel_file = 'output.xlsx'
        df.to_excel(excel_file, index=False)  # Écrire le DataFrame dans un fichier Excel
        return f"Conversion en Excel réussie. Fichier '{excel_file}' créé."
    

def convert_tosql(data, table_name):
        json_data = json.loads(data)  # Charger les données JSON en Python
        df = pd.DataFrame(json_data)  # Créer un DataFrame à partir des données
        from sqlalchemy import create_engine
        engine = create_engine('sqlite:///:memory:')  # Créer une connexion SQL en mémoire
        df.to_sql(table_name, con=engine, index=False)  # Écrire le DataFrame dans une table SQL
        return f"Conversion en SQL réussie. Données écrites dans la table '{table_name}'."
    

def convert_toxml(data):
        json_data = json.loads(data)  # Charger les données JSON en Python
        df = pd.DataFrame(json_data)  # Créer un DataFrame à partir des données
        xml_file = 'output.xml'
        df.to_xml(xml_file, index=False)  # Écrire le DataFrame dans un fichier XML
        return f"Conversion en XML réussie. Fichier '{xml_file}' créé."
    
    
def convert_to_sql_file(data, table_name, sql_file):
        json_data = json.loads(data)  # Charger les données JSON en Python
        rows = []
        for record in json_data:
            columns = ', '.join(record.keys())
            values = ', '.join([f"'{v}'" if isinstance(v, str) else str(v) for v in record.values()])
            rows.append(f"INSERT INTO {table_name} ({columns}) VALUES ({values});")
        with open(sql_file, 'w') as f:
            f.write('\n'.join(rows))

        return f"Conversion en fichier SQL réussie. Fichier '{sql_file}' créé."
    

# Convertir les données en CSV
print(convert_tocsv(json.dumps(data)))

# Convertir les données en Excel
print(convert_to_excel(json.dumps(data)))

# Convertir les données en SQL (en spécifiant le nom de la table)
print(convert_tosql(json.dumps(data), table_name='people'))

# Convertir les données en XML
print(convert_toxml(json.dumps(data)))

# Nom du fichier SQL de sortie
sql_output_file = 'output.sql'

# Nom de la table dans le fichier SQL
table_name = 'people'

# Convertir les données en fichier SQL
result = convert_to_sql_file(json.dumps(data), table_name, sql_output_file)
print(result)

