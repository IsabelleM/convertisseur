DROP DATABASE IF EXISTS authors;

CREATE DATABASE authors;

CREATE TABLE author (
      author varchar(56),
      title varchar(255),
      genre varchar(56),
      price int,
      publish_date DATE,
      description TEXT
);

INSERT INTO sql (author, title, genre, price, publish_date, description) VALUES ('Gambardella, Matthew', 'XML Developer"s Guide', 'Computer', '44.95', '2000-10-01', 'An in-depth look at creating applications 
      with XML.');
INSERT INTO sql (author, title, genre, price, publish_date, description) VALUES ('Ralls, Kim', 'Midnight Rain', 'Fantasy', '5.95', '2000-12-16', 'A former architect battles corporate zombies, 
      an evil sorceress, and her own childhood to become queen 
      of the world.');
INSERT INTO sql (author, title, genre, price, publish_date, description) VALUES ('Corets, Eva', 'Maeve Ascendant', 'Fantasy', '5.95', '2000-11-17', 'After the collapse of a nanotechnology 
      society in England, the young survivors lay the 
      foundation for a new society.');
INSERT INTO sql (author, title, genre, price, publish_date, description) VALUES ('Corets, Eva', 'Oberon"s Legacy', 'Fantasy', '5.95', '2001-03-10', 'In post-apocalypse England, the mysterious 
      agent known only as Oberon helps to create a new life 
      for the inhabitants of London. Sequel to Maeve 
      Ascendant.');